#!/bin/bash

CLUSTER_ALIAS=${1:-test-1}
BUCKET_NAME="replix-state-store-${CLUSTER_ALIAS}"

REGION_NAME=us-east-1
VERSION_ON=true
#ENCRYPT_CFG='{"Rules":[{"ApplyServerSideEncryptionByDefault":{"SSEAlgorithm":"AES256"}}]}'

function print_export_vars()
{
    echo "export KOPS_REGION=${REGION_NAME}"
    echo "export KOPS_STATE_BUCKET='${BUCKET_NAME}'"
    echo "export KOPS_STATE_STORE='s3://${BUCKET_NAME}'"
}

if [[ -z "`aws s3api list-buckets | grep ${BUCKET_NAME}`" ]]; then
    print_export_vars
    echo "# ${KOPS_STATE_STORE} does not exists"
    exit 1
fi

if [[ "${REGION_NAME}" != "us-east-1" ]]; then
    EXTRA_ARGS+="--create-bucket-configuration LocationConstraint=${REGION_NAME}"
fi

if [[ "${VERSION_ON}" == true ]]; then
    aws s3api put-bucket-versioning \
        --bucket ${BUCKET_NAME} \
        --versioning-configuration Status=Disabled
fi

aws s3api delete-bucket \
    --bucket ${BUCKET_NAME} \
    --region ${REGION_NAME} || exit $?

