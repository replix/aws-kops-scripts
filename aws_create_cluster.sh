#!/bin/bash

if [[ $# == 0 ]]; then
    echo "Usage: `basename $0` <cluster-name-prefix> <region>"
    exit 1
fi

CLUSTER_ALIAS=${1:-demo}
CLUSTER_REGION=${2:-us-east-2}

CLUSTER_NODE_CNT=2

CLUSTER_NAME="${CLUSTER_ALIAS}.k8s.local"
KOPS_STATE_BUCKET="replix-state-store-${CLUSTER_ALIAS}"
export KOPS_STATE_STORE="s3://${KOPS_STATE_BUCKET}"

echo "create state store for ${CLUSTER_ALIAS} ..."
./aws_create_state.sh ${CLUSTER_ALIAS} ${CLUSTER_REGION} off off

function print_export_vars()
{
    echo "export CLUSTER_NAME=${CLUSTER_NAME}"
    echo "export CLUSTER_REGION=${CLUSTER_REGION}"
    [[ -n "${CLUSTER_ZONES}" ]] && echo "export CLUSTER_ZONE=${CLUSTER_ZONES}"
    echo "export CLUSTER_NODE_CNT=${CLUSTER_NODE_CNT}"
    echo "export KOPS_STATE_STORE=${KOPS_STATE_STORE}"
}

function update_cluster()
{
    KOPS_UPDATE_CMD="kops update cluster --name ${CLUSTER_NAME} --state ${KOPS_STATE_STORE} --yes"
    echo ${KOPS_UPDATE_CMD}
    ${KOPS_UPDATE_CMD}
}

function validate_cluster()
{
    KOPS_VALIDATE_CMD="kops validate cluster --name ${CLUSTER_NAME} --state ${KOPS_STATE_STORE}"
    echo ${KOPS_VALIDATE_CMD}
    ${KOPS_VALIDATE_CMD}
}

GET_CLUSTER_CMD="kops get cluster ${CLUSTER_NAME} --state ${KOPS_STATE_STORE}"
echo ${GET_CLUSTER_CMD}
cluster_info=`${GET_CLUSTER_CMD}`
if [[ $? == 0 ]]; then
    echo "# cluster ${CLUSTER_NAME} already exists"
    validate_cluster
    CLUSTER_ZONE=`echo ${cluster_info} | tail -n 1 | awk '{print $3}'`
    print_export_vars
    exit 1
fi

echo "create cluster: ${CLUSTER_NAME}"

# meanwhile single region only
REGIONS_LIST="${CLUSTER_REGION}"

for region in ${REGIONS_LIST}; do
    ZONES_LIST=`aws ec2 describe-availability-zones --region ${region} \
        | grep ZoneName | awk '{print $2}'` || exit $?

    ZONES_ARR=(${ZONES_LIST})
    if [[ ${#ZONES_ARR[*]} == 0 ]]; then
        echo "#${region}: no zones found"
        exit 1
    fi

    ZONE="${ZONES_ARR[0]}"
    ZONE="${ZONE%%\",}"
    ZONE="${ZONE##\"}"

    [[ -n "${CLUSTER_ZONES}" ]] && CLUSTER_ZONES+=","
    CLUSTER_ZONES+="${ZONE}"
done

kops create cluster ${CLUSTER_NAME} \
    --zones ${CLUSTER_ZONES} \
    --state ${KOPS_STATE_STORE} \
    --node-count ${CLUSTER_NODE_CNT} \
    --cloud aws \
    || exit $?

  #--node-size $NODE_SIZE \
  #--master-size $MASTER_SIZE \
  #--master-zones $ZONES \
  #--networking weave \
  #--topology private \
  #--bastion="true" \
  #--yes

update_cluster
validate_cluster
print_export_vars
