#!/bin/bash

if [[ $# == 0 ]]; then
    echo "Usage: `basename $0` <cluster-name-prefix> <region> <versioned:on|off> <encrypted:on|off>"
    exit 1
fi

CLUSTER_ALIAS=${1:-demo}
REGION_NAME=${2:-us-east-2}
VERSIONED=${3:-on}
ENCRYPTED=${4:-off}

if [[ "${VERSIONED}" != "on" && "${VERSIONED}" != "off" ]]; then
    echo "VERSIONED must be 'off' or 'on'"
    exit 1
fi
if [[ "${ENCRYPTED}" != "on" && "${ENCRYPTED}" != "off" ]]; then
    echo "ENCRYPTED must be 'off' or 'on'"
    exit 1
fi

BUCKET_NAME="replix-state-store-${CLUSTER_ALIAS}"
ENCRYPT_CFG='{"Rules":[{"ApplyServerSideEncryptionByDefault":{"SSEAlgorithm":"AES256"}}]}'

function print_export_vars()
{
    echo "export KOPS_REGION=${REGION_NAME}"
    echo "export KOPS_STATE_BUCKET='${BUCKET_NAME}'"
    echo "export KOPS_STATE_STORE='s3://${BUCKET_NAME}'"
}

if [[ -n "`aws s3api list-buckets | grep ${BUCKET_NAME}`" ]]; then
    print_export_vars
    echo "# store ${BUCKET_NAME} already exists"
    echo
    exit 1
fi

if [[ "${REGION_NAME}" != "us-east-1" ]]; then
    EXTRA_ARGS+="--create-bucket-configuration LocationConstraint=${REGION_NAME}"
fi


aws s3api create-bucket \
    --bucket ${BUCKET_NAME} \
    --region ${REGION_NAME} ${EXTRA_ARGS} || exit $?

if [[ "${VERSIONED}" == "on" ]]; then
    aws s3api put-bucket-versioning \
        --bucket ${BUCKET_NAME} \
        --versioning-configuration Status=Enabled
fi

if [[ "${ENCRYPTED}" == "on" ]]; then
    aws s3api put-bucket-encryption \
        --bucket ${BUCKET_NAME} \
        --server-side-encryption-configuration ${ENCRYPT_CFG}
fi

print_export_vars
