#!/bin/bash

if [[ $# == 0 ]]; then
    echo "Usage: `basename $0` <cluster-name-prefix> <region>"
    exit 1
fi

CLUSTER_ALIAS=${1:-demo}
CLUSTER_REGION=${2:-us-east-2}

CLUSTER_NAME="${CLUSTER_ALIAS}.k8s.local"
KOPS_STATE_BUCKET="replix-state-store-${CLUSTER_ALIAS}"
KOPS_STATE_STORE="s3://${KOPS_STATE_BUCKET}"

kops delete cluster ${CLUSTER_NAME} --state ${KOPS_STATE_STORE} --yes
