#!/bin/bash

MY_NAME="nezhinsky"

export AWS_ACCESS_KEY_ID=$(aws configure get aws_access_key_id) || \
    aws configure || exit $?

export AWS_SECRET_ACCESS_KEY=$(aws configure get aws_secret_access_key)

USERS_LIST=`aws iam list-users`
MY_USER=`echo "${USERS_LIST}" | grep "UserName" | grep "${MY_NAME}" | awk '{print $2}'`
if [[ -n "${MY_USER}" ]]; then
    MY_USER="${MY_USER%%\",}"
    MY_USER="${MY_USER##\"}"
    #echo "${MY_USER}"
else
    echo "list-users failed"
    exit 1
fi

echo "export AWS_USER_NAME=${MY_USER}"
echo "export AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}"
echo "export AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}"
