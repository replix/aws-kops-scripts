# aws-kops-scripts

Scripts for provisioning K8S clusters on AWS using kops

## Scripts List

### Main Scripts
* aws_create_cluster.sh
* aws_delete_cluster.sh

### Internal Scripts
* aws_get_user.sh
* aws_create_state.sh
* aws_delete_state_bucket.sh

## Create Cluster
Use `aws_create_cluster.sh`, it will create state bucket and deploy all required objects.

Example:
```bash
./aws_create_cluster.sh test-1 us-east-2
```

Note that the first argument is the cluster name prefix, the actual name in the above case will be `test-1.k8s.local`.

## Delete Cluster
Use `aws_delete_cluster.sh`, it will delete the entire cluster and all the associated objects, including the state bucket.

Example:
```bash
./aws_delete_cluster.sh test-2 us-east-2
```

